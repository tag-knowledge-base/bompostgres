""" tasks.py """


import os
from pathlib import Path
import sys
import invoke


DOCKER_REPO = os.getenv("DOCKER_REPOSITORY", "206.189.186.153:8083")
APP_VERSION = os.getenv("APP_VERSION", "1.0.0")
APP_NAME = os.getenv("APP_NAME", "bompostgres")
USER = os.getenv("PYPI_USERNAME")
PASSWORD = os.getenv("PYPI_PASSWORD")


@invoke.task
def build(c):
    """
    Build package.
    """
    c.run("docker build . "
        "--build-arg POSTGRES_USER=$POSTGRES_USER "
        "--build-arg POSTGRES_PASSWORD=$POSTGRES_PASSWORD "
        f"-t {DOCKER_REPO}/{APP_NAME}:{APP_VERSION}"
    )


@invoke.task()
def publish(c):
    """
    Publish package.
    """
    c.run(f"docker login -u {USER} -p {PASSWORD} {DOCKER_REPO}")
    c.run(f"docker push {DOCKER_REPO}/{APP_NAME}:{APP_VERSION}")
